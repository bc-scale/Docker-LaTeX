# ------------------------------------------------------------------------------
# Start from the official Alpine Docker Image
# ------------------------------------------------------------------------------
FROM alpine:latest

RUN set -x && \
    # Upgrade the OS
    apk -U upgrade && \
    # Install LaTeX, Gnuplot, Graphviz, ImageMagick
    apk add --no-cache \
        vim \
        make \
        git \
        texlive-full \
        gnuplot \
        graphviz \
        ttf-freefont \
        imagemagick
